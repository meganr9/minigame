--GROUP MEMBER NAMES: Shane Roberts, Megan Reiffer, Megan Corson, Lori White
--LAST MODIFIED: November 16th, 2016
--This is scene where the user plays the game higher or lower

local composer = require( "composer" )
local widget = require( "widget" )
local game = require("game")
local json = require( "json" )
local cards = require("cards")
local scene = composer.newScene()


local currentBack = 1

--Back Images
local cardBackImages = {
    "RedDefault.png",
    "Armory.png",
    "Cog.png",
    "MidSummer.png",
    "tarot.png",
    "twilight.png"
}
--End Back Images]]

--[[

    The Scene for the Higher/Lower game,
    Contains two game modes deck and endless 
    deck-Go through just one deck of cards
    endless-Keep guessing until you quit

]]


-- -----------------------------------------------------------------------------------
-- Code outside of the scene event functions below will only be executed ONCE unless
-- the scene is removed entirely (not recycled) via "composer.removeScene()"
-- -----------------------------------------------------------------------------------

--[[
    Get's highScore based on saved json file (scores.json)
    Precondition: n/a
    Postcondition: n/a
    Parameters: None
    Returns: (Numbers) The highest score
]]
local function getHighScore()

    local scoresTable = {}

    local filePath = system.pathForFile( "scores.json", system.DocumentsDirectory )

    local file = io.open( filePath, "r" )

    if file then
        local contents = file:read( "*a" )
        io.close( file )
        scoresTable = json.decode( contents )
    end

    return scoresTable[1] or 0

end

--[[
    moves the given card to the left of the screen
    card - the display object to move to the left of the screen (width*.6 of object)
    Precondition: card is a dispaly object to the right of the screen
    Postcondition: card moves to it's width *.6 in 500s 
    with a 4s delay before execution.
    @param: card The Display Object to move to the left of the screen
    @return: n/a
]]
local function moveCardLeft(card)

    transition.to( card, {
        delay = 400,
        time = 500,
        transition = easing.outBack ,
        x = card.width *.6} )
end

-- -----------------------------------------------------------------------------------
-- Scene event functions
-- -----------------------------------------------------------------------------------

-- create()
--[[

    This function is called when the scene is first created but has not yet appeared on screen.
    It takes in the user's choice of whether the next card is higher or lower and saves it while also calling the whichGame function.
    parameters: event Holds different parameters of current aspects of the screen
    return: n/a
    
]]
function scene:create( event )
    local gameType = event.params.gameType == "deck" and 0 or 1 
    local score = 0 
    local streak = 0
    local bestStreak = 0
    local highScore = getHighScore()
    game.start(gameType)

    local higherButton
    local lowerButton

    local sceneGroup = self.view
    -- Code here runs when the scene is first created but has not yet appeared on screen

    local background = display.newRect( sceneGroup, display.contentCenterX, display.contentCenterY, display.actualContentWidth, display.actualContentHeight)
    background:setFillColor( {
        type="gradient",
        color1={ 0.9, 0.2, 0.2}, color2={ 0, 0.8, 0.8}, direction="down"
    } )
 
     local highScoreText = display.newText(sceneGroup, "HighScore:"..highScore, display.contentCenterX*.4, 50, native.systemFont, 16 )
    highScoreText:setFillColor( 1, 1, 1)

     local resultText = display.newText(sceneGroup, "", display.contentCenterX, 350, native.systemFont, 16 )
    resultText:setFillColor( 1, 1, 1)


     local scoreText = display.newText(sceneGroup, "Score: 0", display.contentCenterX, 50, native.systemFont, 16 )
    scoreText:setFillColor( 1, 1, 1 )

    local streakText = display.newText(sceneGroup, "Streak: 0", display.contentCenterX + 90, 50, native.systemFont, 16 )
    scoreText:setFillColor( 1, 1, 1 )


    local firstCard = display.newImageRect( sceneGroup, "cards/"..cards.getCardImage(game.getCurrentCard()),500*(display.actualContentWidth/display.actualContentHeight)*.4, 726*(display.actualContentWidth/display.actualContentHeight)*.4 )
    firstCard.x = firstCard.width *.6
    firstCard.y = display.contentCenterY

    --START CARD BACK IMAGE CREATION
    local deck = display.newImageRect( sceneGroup, cardBackImages[currentBack], 500*(display.actualContentWidth/display.actualContentHeight)*.4, 726*(display.actualContentWidth/display.actualContentHeight)*.4  )
    deck.x = deck.width*2.25
    deck.y = display.contentCenterY
    --END CARD BACK IMAGE CREATION
    
    --[[
        Record current score and go to the HighScores scene
        Parameters: event The event of the button
        Returns: n/a
        Uses higherButton, lowerButton, and score
    ]]
    local function gameFinishedEvent( event )
        if (event == nil or event.phase == "ended") then
            if (higherButton and lowerButon) then
                higherButton:setEnabled( false )
                lowerButton:setEnabled( false )
                higherButton:setFillColor( 0.2, 0.2, 0.2)
                lowerButton:setFillColor( 0.2, 0.2, 0.2)
            end
            composer.setVariable( "finalScore", score )
            composer.setVariable("bestStreak", bestStreak)
            composer.removeScene( "highscores" )
            composer.gotoScene( "highscores", { time=800, effect="crossFade" } )
        end
        
    end

    --[[
        START CHANGE CARD BACK EVENT
        Cycles through card back images whenever the change card back button is pressed
        Parameters: event The event of the button being pressed
        Returns: n/a
    ]]
    local function nextCardBack(event)
        currentBack = currentBack + 1

        if (currentBack > 6) then
            currentBack = 1
        end

        deck = display.newImageRect( sceneGroup, cardBackImages[currentBack], 500*(display.actualContentWidth/display.actualContentHeight)*.4, 726*(display.actualContentWidth/display.actualContentHeight)*.4  )
        deck.x = deck.width*2.25
        deck.y = display.contentCenterY
    end
    --END CHANGE CARD BACK

    --[[
        When the higher or lower buton is clicked,
        update score, move card, notify user of score update,
        (if no more cards finish game instead)
        Parameters: event The event of the button being pressed (lowerButon and higherButton)
        Returns: n/a
        Uses higherButon, lowerButton, cards, game, score, scoreText, gameType, resultText, and deck 
    ]]
    local function higherLowerButtonEvent( event )
        if ( "ended" == event.phase ) then
            local id = event.target.id -- "higher" or "lower"
            local usersAnswer = (id == "higher") and 1 or 0

            if (cards.hasNextCard()) then


                --show next card
                local nextCard = display.newImageRect( sceneGroup, "cards/"..cards.getCardImage(game.getNextCard()),500*(display.actualContentWidth/display.actualContentHeight)*.4, 726*(display.actualContentWidth/display.actualContentHeight)*.4 )
                nextCard.x = display.actualContentWidth - nextCard.width *.6
                nextCard.y = display.contentCenterY


                -- Notify user of answer
                local isAnsweredCorrectly =  game.highOrLow(usersAnswer)
                if (isAnsweredCorrectly) then
                    resultText.text = "Correct! +1 points!"
                    score = score + 1
                    streak = streak + 1
                    scoreText.text = "Score: "..score
                    streakText.text = "Streak: "..streak
                else
                    resultText.text = "incorrect!"
                    if (gameType == 1) then -- endless mode
                        print("heere")
                        higherButton:setEnabled( false )
                        lowerButton:setEnabled( false )
                        higherButton:setFillColor( 0.2, 0.2, 0.2)
                        lowerButton:setFillColor( 0.2, 0.2, 0.2)

                        timer.performWithDelay( 2000, function(event )
                            gameFinishedEvent(nil) end )        
                    end 

                    if (streak > bestStreak) then
                        bestStreak = streak
                    end
                    streak = 0
                    streakText.text = "Streak: "..streak
                end

                game.upDate()

                --hide deck if their isn't a next card
                if (not cards.hasNextCard()) then
                    deck.isVisible = false

                end

                moveCardLeft(nextCard)

            else -- game finished
                cards.resetShuffleIndex()
                gameFinishedEvent(nil)
            end
        end
    end

    higherButton = widget.newButton(
        {
            left = display.contentCenterX - 110,
            top = display.contentCenterY*1.6,
            id = "higher",
            label = "Higher",
            labelColor = { default={ 1, 1, 1 }, over={ 0, 0, 0, 0.5 } },
            onEvent = higherLowerButtonEvent,
                    -- Properties for a rounded rectangle button
            shape = "roundedRect",
            width = 100,
            height = 40,
            cornerRadius = 2,
            fillColor = { default={.5,.5,.5,0.5}, over={1,0.1,0.7,0.3} },


        }
    )
    sceneGroup:insert( higherButton)

    lowerButton = widget.newButton(
        {
            left = display.contentCenterX + 10,
            top = display.contentCenterY*1.6, --1.7
            id = "lower",
            label = "Lower",
            labelColor = { default={ 1, 1, 1 }, over={ 0, 0, 0, 0.5 } },
            onEvent = higherLowerButtonEvent,
                    -- Properties for a rounded rectangle button
            shape = "roundedRect",
            width = 100,
            height = 40,
            cornerRadius = 2,
            fillColor = { default={.5,.5,.5,0.5}, over={1,0.1,0.7,0.3} },

        }
    )
    sceneGroup:insert( lowerButton)

    local cancelButton = widget.newButton(
        {
            left = display.contentCenterX - 50,
            top = display.contentCenterY*1.8,
            id = "cancel",
            label = "End Game",
            labelColor = { default={ 1, 1, 1 }, over={ 0, 0, 0, 0.5 } },
            onEvent = gameFinishedEvent,
                    -- Properties for a rounded rectangle button
            shape = "roundedRect",
            width = 100,
            height = 40,
            cornerRadius = 2,
            fillColor = { default={.5,.5,.5,0.5}, over={1,0.1,0.7,0.3} },

        }
    )
    sceneGroup:insert( cancelButton)

    --Change Card Back BUTTON CODE
    local cardButton = widget.newButton(
        {
            left = 168,
            top = -40,
            id = "cardButton",
            label = "Change Card Back",
            labelColor = { default={ 1, 1, 1 }, over={ 0, 0, 0, 0.5 } },
            onRelease = nextCardBack,
                    -- Properties for a rounded rectangle button
            shape = "roundedRect",
            width = 150,
            height = 40,
            cornerRadius = 2,
            fillColor = { default={.5,.5,.5,0.5}, over={1,0.1,0.7,0.3} },

        }
    )
    sceneGroup:insert(cardButton)
    --Change Card Back

end


-- show()
--[[

    This function is called when the scene is first created and has appeared on screen.
    parameters: event Holds different parameters of current aspects of the screen
    return: n/a
    
]]
function scene:show( event )

    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then
        -- Code here runs when the scene is still off screen (but is about to come on screen)

    elseif ( phase == "did" ) then
        -- Code here runs when the scene is entirely on screen

    end
end


-- hide()
--[[

    This function is called when the scene is hidden on screen.
    parameters: event Holds different parameters of current aspects of the screen
    return: n/a
    
]]
function scene:hide( event )

    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then
        -- Code here runs when the scene is on screen (but is about to go off screen)

    elseif ( phase == "did" ) then
        -- Code here runs immediately after the scene goes entirely off screen

    end
end


-- destroy()
--[[

    This function is called when the scene is destroyed.
    parameters: event Holds different parameters of current aspects of the screen
    return: n/a
    
]]
function scene:destroy( event )

    local sceneGroup = self.view
    -- Code here runs prior to the removal of scene's view

end


-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------

return scene