# README #
Todo:


*    Table for the cards
*    GUI: higher and lower buttons; image of the deck and current card; pretty background
*    Show next card after higher/lower selected
*    checker for if player selects higher/lower to determine if they're correct
*    Module/Table to shuffle deck and get the next card

### What is this repository for? ###

* Quick summary
    Multi-platform (Corona SDK w/ Lua) phone game of the Higher or lower card game.  If the user guesses right, they get a point. If they don’t guess right, they don’t get any points. Points will equal prizes. Each tier of points will get a different type of prize, like a deck color, background color, or perhaps a gif of something cute to sit in the window to keep the user company. 

* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
   Corona SDK, Lua, (if sublime corona sdk plugin as well)
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions
Run in Corona Simulator by selecting project folder


### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact