local composer = require( "composer" )
local widget = require("widget")
local scene = composer.newScene()

-- -----------------------------------------------------------------------------------
-- Code outside of the scene event functions below will only be executed ONCE unless
-- the scene is removed entirely (not recycled) via "composer.removeScene()"
-- -----------------------------------------------------------------------------------


local function gotoEndlessModeGame()
    composer.gotoScene( "game_original", {
        effect = "fade",
        time = 400,
        params = {
            gameType = "endless"
        }
    } )
end

local function gotoDeckModeGame()
    composer.gotoScene( "game_original" , {
        effect = "fade",
        time = 400,
        params = {
            gameType = "deck"
        }
    })
end


-- -----------------------------------------------------------------------------------
-- Scene event functions
-- -----------------------------------------------------------------------------------

-- create()
function scene:create( event )

    local sceneGroup = self.view
    -- Code here runs when the scene is first created but has not yet appeared on screen
    
    local background = display.newRect( sceneGroup, display.contentCenterX, display.contentCenterY, display.actualContentWidth, display.actualContentHeight)
    background:setFillColor( {
        type="gradient",
        color1={ 0.9, 0.2, 0.2}, color2={ 0, 0.8, 0.8}, direction="down"
    } )


    local title = display.newText( sceneGroup, "Play,\nHigher or Lower", display.contentCenterX, 200, native.systemFont, 44 )
    title:setFillColor( 0.82, 0.86, 1 )

    -- Create the widget
    local playEndlessModeButton = widget.newButton(
        {
            label = "Endless Mode",
            onEvent = gotoEndlessModeGame,
            emboss = false,
            x = display.contentCenterX,
            y = display.contentHeight - 150,
            labelColor = { default={ 0, 0, 0 }, over={ 0, 0, 0, 0.5 } },
            -- Properties for a rounded rectangle button
            shape = "roundedRect",
            width = 200,
            height = 40,
            cornerRadius = 2,
            fillColor = { default={ 0.82, 0.86, 1 }, over={0.2} }

    })
    sceneGroup:insert( playEndlessModeButton)


    -- Create the widget
    local playDeckModeButton = widget.newButton(
        {
            label = "Deck Mode",
            onEvent = gotoDeckModeGame,
            emboss = false,
            x = display.contentCenterX,
            y = display.contentHeight - 100,
            labelColor = { default={ 0, 0, 0 }, over={ 0, 0, 0, 0.5 } },
            -- Properties for a rounded rectangle button
            shape = "roundedRect",
            width = 200,
            height = 40,
            cornerRadius = 2,
            fillColor = { default={ 0.82, 0.86, 1 }, over={0.2} }

    })
    sceneGroup:insert( playDeckModeButton)


    local function gotoHighScoreScene( event )
        composer.gotoScene( "highscores", { time=800, effect="crossFade" } )
    end

    -- Create the widget
    local highScoresButton = widget.newButton(
        {
            label = "HighScores",
            onEvent = gotoHighScoreScene,
            emboss = false,
            x = display.contentCenterX,
            y = display.contentHeight - 50,
            labelColor = { default={ 0, 0, 0 }, over={ 0, 0, 0, 0.5 } },
            -- Properties for a rounded rectangle button
            shape = "roundedRect",
            width = 200,
            height = 40,
            cornerRadius = 2,
            fillColor = { default={ 0.82, 0.86, 1 }, over={0.2} }

    })
    sceneGroup:insert( highScoresButton)


end


-- show()
function scene:show( event )

    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then
        -- Code here runs when the scene is still off screen (but is about to come on screen)

    elseif ( phase == "did" ) then
        -- Code here runs when the scene is entirely on screen

    end
end


-- hide()
function scene:hide( event )

    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then
        -- Code here runs when the scene is on screen (but is about to go off screen)

    elseif ( phase == "did" ) then
        -- Code here runs immediately after the scene goes entirely off screen

    end
end


-- destroy()
function scene:destroy( event )

    local sceneGroup = self.view
    -- Code here runs prior to the removal of scene's view

end


-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------

return scene