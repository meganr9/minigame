--[[

    Group Members: Lori White, Megan Reiffer, Megan Corson, Cole Hulse and Shane Roberts
    Date: 11/15/2016
    Author: Lori White
    This module represents the game play for Higher or Lower based on which option the user selects.
    There are two types of game play endless mode and regular mode for the user to select.

]]

local game = {}
local userChoice = -1
local gameChoice = 0
local currentCard = ""
local nextCard = ""
local answer = -1
local check = false
local cardModule = require("cards")

--[[

    This private function starts the game based on the users game play selection.
    If the user picked endless mode then calls the random pick function from the card module to get the current card and the next card.
    If the user picked regular mode then calls the draw card function from the card module to get the current card and the next card.
    parameters: none
    return: none

]]

local function start()
    -- body
    if gameChoice == 1 then
        currentCard = cardModule.randomPick()
        nextCard = cardModule.randomPick()
    else
        cardModule.shuffleDeck()
        currentCard = cardModule.drawCard()
        nextCard = cardModule.drawCard()
    end
end

--[[

    This private function compares the current card's value with the next card's value.
    If the current card's value is less than or equal to next card's value then the answer is set to 1.
    If the current card's value is greater than next card's value then the answer is set to 0.
    parameters: none
    return: none
    
]]

local function compareTo()
    -- body
    if cardModule.getValueOfCard(currentCard) <= cardModule.getValueOfCard(nextCard) then
        answer = 1
    else
        answer = 0
    end
end

--[[

    This private function checks to see if the user is correct at guessing if the next card is higher or lower than the current card.
    If the user picked correct then check is set to true otherwise set to false.
    parameters: none
    return: none
    
]]

local function equals()
    -- body
    check = answer == userChoice
end

--[[

    This private function calls the compare to function and equals function based on which game play the user selected.
    If the user picked regular mode then checks if the next card is the end of the deck and if it is then no function is called.
    parameters: none
    return: none
    
]]

local function whichGame()
    -- body
    if gameChoice == 1 then
        compareTo()
        equals()
    else
        if nextCard ~= "end" then
            compareTo()
            equals()
        end
    end
end

--[[

    This function starts the game based on the users game play selection.
    It takes in the user's game play choice and saves it while also calling the start function.
    parameters: gameType The user's choice for type of game play
    return: none
    
]]

function game.start(gameType)
    gameChoice = gameType
    start()
end

--[[

    This function checks the user's choice of whether the next card is higher or lower.
    It takes in the user's choice of whether the next card is higher or lower and saves it while also calling the whichGame function.
    parameters: userInput The user's choice of whether the next card is higher or lower
    return: check Whether the user was correct or not
    
]]

function game.highOrLow(userInput)
    userChoice = userInput
    
    whichGame()

    return check
end

--[[

    This function updates the game based on if the next cardis the end of the deck.
    If the next card is not the end of the deck then current card is set to the next card.
    Then based on the users game play selection the next card is chosen.  
    If the user picked endless mode then calls the random pick function from the card module for the next card.
    If the user picked regular mode then calls the draw card function from the card module for the next card.
    Otherwise the current card is set to end.
    parameters: none
    return: none
    
]]

function game.upDate()
    -- body
    if nextCard ~= "end" then
        currentCard = nextCard
        if  gameChoice == 1 then
              nextCard = cardModule.randomPick()
        else
            nextCard = cardModule.drawCard()
        end
    else
        currentCard = "end"
    end
end

--[[

    This function returns the current card.
    parameters: none
    return: currentCard The card that is face up to the user
    
]]

function game.getCurrentCard()
    -- body
    return currentCard
end     

--[[

    This function returns the next card.
    parameters: none
    return: nextCard The card that is face down on the top of the deck of cards.
    
]]

function game.getNextCard()
    return nextCard
end


return game