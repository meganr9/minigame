--GROUP MEMBER NAMES: Shane Roberts, Megan Reiffer, Megan Corson, Lori White
--LAST MODIFIED: November 14th, 2016
--The cards module stores and simulates a deck of cards. This includes many card related functions such as random draw, drawing from a shuffled deck, shuffling a deck, etc.
--Shuffled card deck is handled by maintaining an index, shuffleIndex, that tells us which card in the shuffledCardDeck we last used. We iterate this until the index is at 52, at which point we have used up all the cards of the deck.

local cardModule = {}
local deckOfCards = {"1S", "2S", "3S", "4S", "5S", "6S", "7S", "8S", "9S", "10S", "11S", "12S", "13S", "1D", "2D", "3D", "4D", "5D", "6D", "7D", "8D", "9D", "10D", "11D", "12D", "13D", "1C", "2C", "3C", "4C", "5C", "6C", "7C", "8C", "9C", "10C", "11C", "12C", "13C", "1H", "2H", "3H", "4H", "5H", "6H", "7H", "8H", "9H", "10H", "11H", "12H", "13H"}
local cardImages = {"ace_of_spades.png", "2_of_spades.png", "3_of_spades.png", "4_of_spades.png", "5_of_spades.png", "6_of_spades.png", "7_of_spades.png", "8_of_spades.png", "9_of_spades.png", "10_of_spades.png", "jack_of_spades.png", "queen_of_spades.png", "king_of_spades.png", "ace_of_diamonds.png", "2_of_diamonds.png", "3_of_diamonds.png", "4_of_diamonds.png", "5_of_diamonds.png", "6_of_diamonds.png", "7_of_diamonds.png", "8_of_diamonds.png", "9_of_diamonds.png", "10_of_diamonds.png", "jack_of_diamonds.png", "queen_of_diamonds.png", "king_of_diamonds.png", "ace_of_clubs.png", "2_of_clubs.png", "3_of_clubs.png", "4_of_clubs.png", "5_of_clubs.png", "6_of_clubs.png", "7_of_clubs.png", "8_of_clubs.png", "9_of_clubs.png", "10_of_clubs.png", "jack_of_clubs.png", "queen_of_clubs.png", "king_of_clubs.png", "ace_of_hearts.png", "2_of_hearts.png", "3_of_hearts.png", "4_of_hearts.png", "5_of_hearts.png", "6_of_hearts.png", "7_of_hearts.png", "8_of_hearts.png", "9_of_hearts.png", "10_of_hearts.png", "jack_of_hearts.png", "queen_of_hearts.png", "king_of_hearts.png"}
local shuffledCardDeck =  {"1S", "2S", "3S", "4S", "5S", "6S", "7S", "8S", "9S", "10S", "11S", "12S", "13S", "1D", "2D", "3D", "4D", "5D", "6D", "7D", "8D", "9D", "10D", "11D", "12D", "13D", "1C", "2C", "3C", "4C", "5C", "6C", "7C", "8C", "9C", "10C", "11C", "12C", "13C", "1H", "2H", "3H", "4H", "5H", "6H", "7H", "8H", "9H", "10H", "11H", "12H", "13H"}
local currentCard = ""
local valueString = ""
local value = 0
local shuffleIndex = 0

--Draw a random card without worrying about deck length or the current order of the deck. In essence - just pick a random card out of a deck of 52 cards. Will not draw the same card twice in a row.
--Parameters: none.
--Returns: The card we drew out of the deck.
--Uses deckOfCards and currentCard
function cardModule.randomPick()
	local repeater = true
	local index

	--This while loop will simply repeat until the value of our randomly chosen card is different from our current card. (so we never will draw the same card twice in a row)
	while repeater == true do
		index = math.random(52)
		if deckOfCards[index] ~= currentCard then
			repeater = false
		end
	end

	--Note: we maintain currentCard every time we call this function. After we run this function, current card is updated internally to our latest chosen card.
	currentCard = deckOfCards[index]
	return currentCard
end

--Shuffle the shuffledCardDeck into random order via choosing 2 random elements of the deck and swapping them 52 times.
--Parameters: none.
--Returns: nothing
--Uses shuffleIndex and shuffledCardDeck.
function cardModule.shuffleDeck()
	shuffleIndex = 0
	local n = 52 --A full deck of cards
    while n > 1 do
	    local k = math.random(52)
	    shuffledCardDeck[n], shuffledCardDeck[k] = shuffledCardDeck[k], shuffledCardDeck[n] --swap the nth element of the deck with our randomly chosen index k.
	    n = n - 1
	 end
end

--Checks to see if we have another card left to draw in the shuffledCardDeck.
--Parameters: None
--Returns: true if we have another card in the deck, false if we do not have another card in the deck.
--Uses: shuffleIndex
function cardModule.hasNextCard()
	return shuffleIndex < 52 --Logically, we have a next card available until we are at card 52 - the last card in the deck.
end

--Draws a card from the shuffledCardDeck
--Parameters: None
--Returns: the card that was drawn from the deck
--Uses: shuffleIndex, shuffledCardDeck, and currentCard.
function cardModule.drawCard()
	--This draws the next card from the deck simply by incrementing our index by 1 and then returning that value.
	shuffleIndex = shuffleIndex + 1
	if (shuffleIndex > 52) then
		return shuffledCardDeck[52] --This is just a failsafe in case we try drawing a card when the deck is already full... but we shouldn't ever be here with good control flow.
	end

	--Note: we maintain currentCard every time we call this function. After we run this function, current card is updated internally to our latest chosen card.
	currentCard = shuffledCardDeck[shuffleIndex]
	return currentCard
end

--Gets the numerical value of a card.
--Parameters: a card
--Returns: the value of the card
--Uses: nothing
function cardModule.getValueOfCard(card)
	valueString = string.sub(card, 1, -2)
	value = tonumber(valueString)
	return value
end

--Finds the image of a card as specified by the name of the card.
--Parameters: the name of a card, cardName.
--Returns: The image of the card as specified by cardName.
--Uses: deckOfCards and cardImages
function cardModule.getCardImage(cardName)
	for index = 1, 52 do
		if deckOfCards[index] == cardName then
			return cardImages[index]
		end
	end
	return cardImages[1] --This is a default fallback, but we should never be here as long as we directly pass the value of a string from deckOfCards.
end

--Resets the index of the shuffled card deck back to 0.
--Parameters: none
--Returns: nothing
--Uses: shuffleIndex
function cardModule.resetShuffleIndex()
	shuffleIndex = 0
end

return cardModule