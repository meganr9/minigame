--GROUP MEMBER NAMES: Shane Roberts, Megan Reiffer, Megan Corson, Lori White
--LAST MODIFIED: November 16th, 2016
--This is scene where the users score is shown, stored and a picture is shown of any achievements made

local composer = require( "composer" )
local widget = require("widget")
local scene = composer.newScene()
local score = 0
local streak = 0

-- -----------------------------------------------------------------------------------
-- Code outside of the scene event functions below will only be executed ONCE unless
-- the scene is removed entirely (not recycled) via "composer.removeScene()"
-- -----------------------------------------------------------------------------------

-- Initialize variables
local json = require( "json" )

local scoresTable = {}

local filePath = system.pathForFile( "scores.json", system.DocumentsDirectory )

--[[
    Loads the scores from the json found in filePath and sets scoresTable
    parameters: none
    return: none
    Uses: filePath, and scoresTable
]]
local function loadScores()

    local file = io.open( filePath, "r" )

    if file then
        local contents = file:read( "*a" )
        io.close( file )
        scoresTable = json.decode( contents )
    end

    if ( scoresTable == nil or #scoresTable == 0 ) then
        scoresTable = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }
    end
end

--[[
    Saves the scores from the scoresTable to the json found in filePath 
    Precondition: scoresTable is set
    parameters: none
    return: none
    Uses: filePath, and scoresTable
]]
local function saveScores()

    for i = #scoresTable, 11, -1 do
        table.remove( scoresTable, i )
    end

    local file = io.open( filePath, "w" )

    if file then
        file:write( json.encode( scoresTable ) )
        io.close( file )
    end
end

--[[
    Goes the menu scene
    Precondition: The scene menu exists
    parameters: n/a
    return: n/a
]]
local function gotoMenu()
    composer.gotoScene( "menu", { time=800, effect="crossFade" } )
end

-- -----------------------------------------------------------------------------------
-- Scene event functions
-- -----------------------------------------------------------------------------------

-- create()
--[[

    This function is called when the scene is first created but has not yet appeared on screen.
    Creates the highscore scene, 
    parameters: event Holds different parameters of current aspects of the screen
    return: n/a
    
]]
function scene:create( event )

    composer.removeScene( "game_original" )

    local sceneGroup = self.view
    -- Code here runs when the scene is first created but has not yet appeared on screen

    -- Load the previous scores
    loadScores()

    -- Insert the saved score from the last game into the table, then reset it
    table.insert( scoresTable, composer.getVariable( "finalScore" ) )
    score = 0
    score = composer.getVariable( "finalScore" )
    streak = composer.getVariable("bestStreak")
    composer.setVariable( "finalScore", 0 )

    -- Sort the table entries from highest to lowest
    local function compare( a, b )
        return a > b
    end
    table.sort( scoresTable, compare )

    -- Save the scores
    saveScores()

    local background = display.newRect( sceneGroup, display.contentCenterX, display.contentCenterY, display.actualContentWidth, display.actualContentHeight)
    background:setFillColor( {
        type="gradient",
        color1={ 0.9, 0.2, 0.2}, color2={ 0, 0.8, 0.8}, direction="down"
    } )

    local highScoresHeader = display.newText( sceneGroup, "High Scores", display.contentCenterX, 100, native.systemFont, 44 )

    for i = 1, 4 do
        if ( scoresTable[i] ) then
            local yPos = 150 + ( i * 56 )

            local rankNum = display.newText( sceneGroup, i .. ")", display.contentCenterX-50, yPos, native.systemFont, 36 )
            rankNum:setFillColor( 0.8 )
            rankNum.anchorX = 1

            local thisScore = display.newText( sceneGroup, scoresTable[i], display.contentCenterX-30, yPos, native.systemFont, 36 )
            thisScore.anchorX = 0
        end
    end

    local menuButton = display.newText( sceneGroup, "Menu", display.contentCenterX, 810, native.systemFont, 44 )
    menuButton:setFillColor( 0.75, 0.78, 1 )
    menuButton:addEventListener( "tap", gotoMenu )


    -- Create the widget
    local menuButton = widget.newButton(
        {
            label = "Menu",
            onEvent = gotoMenu,
            emboss = false,
            x = display.contentCenterX,
            y = display.contentHeight *.9,
            labelColor = { default={ 0, 0, 0 }, over={ 0, 0, 0, 0.5 } },
            -- Properties for a rounded rectangle button
            shape = "roundedRect",
            width = 200,
            height = 40,
            cornerRadius = 2,
            fillColor = { default={ 0.82, 0.86, 1 }, over={0.2} }

    })
    sceneGroup:insert( menuButton)

    --COLE'S CODE FOR ACHIEVEMENTS
    local ach1 = widget.newButton(
    {
        label = "",
        x = display.contentCenterX,
        y = -20,
        shape = "roundedRect",
        width = 230,
        height = 30,
        cornerRadius = 2,
        fillColor = { default={ 0.82, 0.86, 1 } }
    })
    sceneGroup:insert( ach1)

    local ach2 = widget.newButton(
    {
        label = "",
        x = display.contentCenterX,
        y = 20,
        shape = "roundedRect",
        width = 230,
        height = 30,
        cornerRadius = 2,
        fillColor = { default={ 0.82, 0.86, 1 } }
    })
    sceneGroup:insert( ach2)

    local pic1 = display.newImageRect(sceneGroup, "A1.png", 30, 30)
    pic1.x = 20
    pic1.y = -20
    

    local pic2 = display.newImageRect(sceneGroup, "A1.png", 30, 30)
    pic1.x = 20
    pic1.y = 25

    if (score ~= nil) then

        if (score < 5 and score >= 0) 
            then
            pic1:removeSelf()
            ach1:setLabel("Scored under 5 points... :/")
            pic1 = display.newImageRect(sceneGroup, "A1.png", 30, 30)
            pic1.x = 20
            pic1.y = -20
        elseif  (score >= 5 and score < 10) 
            then
            pic1:removeSelf()
            ach1:setLabel("Scored at least 5 points")
            pic1 = display.newImageRect(sceneGroup, "A3.png", 30, 30)
            pic1.x = 20
            pic1.y = -20
        elseif (score >= 10 and score < 20) 
        then
            pic1:removeSelf()
            ach1:setLabel("Scored 10 or more points")
            pic1 = display.newImageRect(sceneGroup, "A5.png", 30, 30)
            pic1.x = 20
            pic1.y = -20
        elseif (score >= 20 and score < 30) 
        then
            pic1:removeSelf()
            ach1:setLabel("Scored 20 or more points")
            pic1 = display.newImageRect(sceneGroup, "A4.png", 30, 30)
            pic1.x = 20
            pic1.y = -20
        elseif (score >= 30 and score < 40) 
        then
            pic1:removeSelf()
            ach1:setLabel("Scored 30 or more points")
            pic1 = display.newImageRect(sceneGroup, "A2.png", 30, 30)
            pic1.x = 20
            pic1.y = -20
        elseif (score >= 40 and score < 52) 
        then
            pic1:removeSelf()
            ach1:setLabel("Scored 40 or more points")
            pic1 = display.newImageRect(sceneGroup, "A6.png", 30, 30)
            pic1.x = 20
            pic1.y = -20
        elseif (score >= 52)
        then
            pic1:removeSelf()
            ach1:setLabel("Scored 52 or more points")
            pic1 = display.newImageRect(sceneGroup, "A7.png", 30, 30)
            pic1.x = 20
            pic1.y = -20
        end
    else
        ach1:removeSelf()
        pic1:removeSelf()
    end

    if (streak ~= nil) then

        if (streak < 3 and streak >= 0) 
            then
            pic2:removeSelf()
            ach2:setLabel("No streak over 3 :c")
            pic2 = display.newImageRect(sceneGroup, "A1.png", 30, 30)
            pic2.x = 20
            pic2.y = 25
        elseif  (streak >= 3 and streak < 5) 
            then
            pic2:removeSelf()
            ach2:setLabel("Streak of 3 or more")
            pic2 = display.newImageRect(sceneGroup, "A3.png", 30, 30)
            pic2.x = 20
            pic2.y = 25
        elseif (streak >= 5 and streak < 7) 
        then
            pic2:removeSelf()
            ach2:setLabel("Streak of 5 or more")
            pic2 = display.newImageRect(sceneGroup, "A5.png", 30, 30)
            pic2.x = 20
            pic2.y = 25
        elseif (streak >= 7 and streak < 10) 
        then
            pic2:removeSelf()
            ach2:setLabel("Streak of 7 or more")
            pic2 = display.newImageRect(sceneGroup, "A4.png", 30, 30)
            pic2.x = 20
            pic2.y = 25
        elseif (streak >= 10 and streak < 15) 
        then
            pic2:removeSelf()
            ach2:setLabel("Streak of 10 or more")
            pic2 = display.newImageRect(sceneGroup, "A2.png", 30, 30)
            pic2.x = 20
            pic2.y = 25
        elseif (streak >= 15 and streak < 20) 
        then
            pic2:removeSelf()
            ach2:setLabel("Streak of 15 or more")
            pic2 = display.newImageRect(sceneGroup, "A6.png", 30, 30)
            pic2.x = 20
            pic2.y = 25
        elseif (streak >= 20)
        then
            pic2:removeSelf()
            ach2:setLabel("Streak of 20 or more")
            pic2 = display.newImageRect(sceneGroup, "A7.png", 30, 30)
            pic2.x = 20
            pic2.y = 25
        end
    else
        ach2:removeSelf()
        pic2:removeSelf()
    end

    --COLE'S CODE FOR ACHIEVEMENTS
    
end


-- show()
--[[

    This function is called when the scene is first created and has appeared on screen.
    parameters: event Holds different parameters of current aspects of the screen
    return: n/a
    
]]
function scene:show( event )

    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then
        -- Code here runs when the scene is still off screen (but is about to come on screen)

    elseif ( phase == "did" ) then
        -- Code here runs when the scene is entirely on screen

    end
end


-- hide()
--[[

    This function is called when the scene is hidden on screen.
    parameters: event Holds different parameters of current aspects of the screen
    return: n/a
    
]]
function scene:hide( event )

    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then
        -- Code here runs when the scene is on screen (but is about to go off screen)

    elseif ( phase == "did" ) then
        -- Code here runs immediately after the scene goes entirely off screen

    end
end


-- destroy()
--[[

    This function is called when the scene is destroyed.
    parameters: event Holds different parameters of current aspects of the screen
    return: n/a
    
]]
function scene:destroy( event )

    local sceneGroup = self.view
    -- Code here runs prior to the removal of scene's view

end


-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------

return scene